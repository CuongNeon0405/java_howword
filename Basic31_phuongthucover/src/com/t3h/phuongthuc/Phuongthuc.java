package com.t3h.phuongthuc;

public class Phuongthuc {
//  overload có tên giống  nhau nhưng đối số truyền vào khác nhau
	 public int sum(int a , int b) {
		return a+b;
	}
	 public long sum(int a , long b) {
		return  a+b;
	}
	 public float sum(float a) {
		return a;
	}
}
