package com.t3h.abstracr;
// khong de tao doi tuong moi la new chuyen phuc vu cho ke thua 
public abstract class Person {
 private String ten ;
 private int age;
public Person() {
	super();
}
public Person(String ten, int age) {
	super();
	this.ten = ten;
	this.age = age;
}
public String getTen() {
	return ten;
}
public void setTen(String ten) {
	this.ten = ten;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
} 

}
