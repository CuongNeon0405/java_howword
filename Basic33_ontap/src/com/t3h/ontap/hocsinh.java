package com.t3h.ontap;

public class hocsinh extends Peson {
	private float d1;
	private float d2;
	
	public hocsinh() {
		super();
	}
	public hocsinh(String ten , int age ,float d1, float d2) {
		super(ten,age);
		
		this.d1 = d1;
		this.d2 = d2;
	}
	public float getD1() {
		return d1;
	}
	public void setD1(float d1) {
		this.d1 = d1;
	}
	public float getD2() {
		return d2;
	}
	public void setD2(float d2) {
		this.d2 = d2;
	}
	public float tongket() {
		return (getD1()+getD2())/2;
	}
	

}
