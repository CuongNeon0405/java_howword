package com.t3h.sv;

public class sinhvien {
  private String  msv ;
  private String  hoten ;
  private String lop;
  private double  dieml1;
  private double dieml2;
  public double  diemtb;
  public sinhvien() {
	// TODO Auto-generated constructor stub
   }
  public sinhvien(String msv,String hoten , String lop , double dieml1,double dieml2) {
	 this.msv = msv;
	 this.hoten =hoten;
	 this.lop = lop;
	 this.dieml1 = dieml1;
	 this.dieml2 = dieml2;
  }
		public String getMsv() {
			return msv;
		}
		public void setMsv(String msv) {
			this.msv = msv;
		}
		public String getHoten() {
			return hoten;
		}
		public void setHoten(String hoten) {
			this.hoten = hoten;
		}
		public String getLop() {
			return lop;
		}
		public void setLop(String lop) {
			this.lop = lop;
		}
		public double getDieml1() {
			return dieml1;
		}
		public void setDieml1(double dieml1) {
			this.dieml1 = dieml1;
		}
		public double getDieml2() {
			return dieml2;
		}
		public void setDieml2(double dieml2) {
			this.dieml2 = dieml2;
		}
		public double getDiemtb() {
			return (getDieml1()+getDieml2())/2;
		}

}
