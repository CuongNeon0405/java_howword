package basic_3_toantu;

public class ToanTu {

	public static void main(String[] args) {
		// cac phep toan so nguyen
       int x = 10;
       int  y = 15;
       int tong = x+y;
       int hieu = y-x;
       int tich = x*y;
       int thuong = x/y;
       int  du = y%x;
       System.out.println(tong);
       System.out.println(hieu);
       System.out.println(tich);
       System.out.println(thuong);
       System.out.println(du);
       // cac phep toan so thuc
       float  f1 = 3.1f;
       float f2 =  4.3f;
       float tongf = f1 +f2;
       System.out.println(tongf);
       // nang cap kieu du lieu
       byte  b = 4;
       short  s=  67;
       int i= 100;
       long  k = 1000l;
       long  tongl  = k+i+s+b; // chi nang cap  duoi len tren  khong  duoc nang cap tren xuong duoi
       int  tongint  =(int) k+i+b ; //chuong trinh bao loi va su dung ep kieu bang tay 
       // ep  kieu
       double d1 = 4.1;
       long tongl1  = (long)d1+k;
       double  tongdo =k+d1;
       System.out.println(tongl1);
       System.out.println(tongdo);
       //  phep gan
       int  t= 10;
       t = t+4; //  phep gan gia tri cua chinh no
       
       System.out.println(t);
       t+=4; // tang 4  lan cho t roi gan gia tri cho no
       System.out.println(t);
	}

}
