package com.t3h.constructor;

public class Book {
     String ten;
     String  mota;
     double price ;
     // constructor khoi tao gia tri trong class
     
     public Book() {
    	 // không có giá trị truyền vào
     }
     public  Book(String ten  , String mota , double  price) {
    	 //có   giá tri  truyền vào
    	 this.ten = ten;
    	 this.mota = mota;
    	 this.price = price;
     }
     
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public String getMota() {
		return mota;
	}
	public void setMota(String mota) {
		this.mota = mota;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
   

	
     
}
